<?php
/**
 * Created by PhpStorm.
 * User: Patryk
 * Date: 16.12.17
 * Time: 12:29
 */

class User
{
    private $login;
    private $email;
    private $sex;
    private $keyRecover;
    private $keyDate;

    private $steamid;
    private $steamName;

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getEmail(): string
    {
        return $this->login;
    }

    public function getSteamID(): int
    {
        return $this->steamid;
    }

    public function getSteamName(): string
    {
        return $this->steamName;
    }

    public function getSex(): string
    {
        if($this->sex == 0) return "Mężczyzna";
        else if($this->sex == 1) return "Kobieta";
    }

    public function login(string $login, string $password)
    {
        try
        {
            $connection = new mysqli('localhost', 'patryk','11441','esport');
            if($connection->connect_errno != 0)
            {
                throw new Exception($connection->connect_errno);
            }
            else
            {
                if($result = $connection->query("SELECT * FROM users WHERE username='$connection->real_escape_strin($login)'"))
                {
                    if($result->num_rows > 0)
                    {
                        $row = $result->fetch_assoc();
                        if(password_verify($password, $row['password']))
                        {
                            $_SESSION['user_uid'] = $row['uid'];
                            $_SESSION['isLogged'] = true;
                        }
                        else
                        {
                            $_SESSION['error_password'] = "Niepoprawne hasło.";
                        }
                    }
                    else
                    {
                        $_SESSION['error_login'] = "Niepoprawna nazwa użytkownia.";
                    }
                    $result->free_result();
                }
                else
                {
                    throw new Exception($connection->error);
                }
                $connection->close();
            }
        }
        catch(Exception $error)
        {
            echo 'Wystapil blad<br>';
            echo 'Number: '.$error->getCode().'<br>';
            echo 'Information: '.$error->getMessage();
        }
    }

    public function register(string $login, string $password, string $password2, string $email, int $sex, bool $rules)
    {
        $validate = true;

        $login = htmlspecialchars('$login', ENT_QUOTES);

        if(!preg_match('/^[a-zA-Z0-9]{3,24}$/D', $login))
        {
            $validate = false;
            $_SESSION['error_register_login'] = "Nazwa uzytkownika moze zawierac tylko litery i cyfry (bez polskich znakow). Zakres znaków wynosi od 3 do 24.";
        }

        if(strlen($password) < 2 || strlen($password) > 24)
        {
            $validate = false;
        }

        if(strcmp($password, $password2))
        {
            $validate = false;
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $validate = false;
        }

        if($sex != 1 || $sex != 0)
        {
            $validate = false;
        }

        if($rules != 1)
        {
            $validate = false;
        }

        if($validate == true)
        {
            try
            {
                $connection = new mysqli('localhost', 'patryk','11441','esport');
                if($connection->connect_errno != 0)
                {
                    throw new Exception($connection->connect_errno);
                }
                else
                {
                    $query = "INSERT INTO users (username, password, email, sex) VALUES ($connection->real_escape_string($login), password_hash($password, PASSWORD_DEFAULT), '$connection->real_escape_string($email)', '$connection->real_escape_string($sex)')";
                    if(!$connection->query($query))
                    {
                        throw new Exception($connection->error);
                    }
                    $connection->close();
                }
            }
            catch(Exception $error)
            {
                echo 'Wystapil blad<br>';
                echo 'Number: '.$error->getCode().'<br>';
                echo 'Information: '.$error->getMessage();
            }
        }
    }

    public function newPassword(int $uid)
    {
        $key = password_hash(rand(0, 999), PASSWORD_DEFAULT);
        try
        {
            $connection = new mysqli('localhost', 'patryk', '11441', 'esport');
            if ($connection->connect_errno != 0) {
                throw new Exception($connection->connect_errno);
            } else {
                if (!$connection->query("UPDATE users SET key_recovery='$key', key_date='now() + INTERVAL 15 MINUTE' WHERE uid='$uid'")) {
                    throw new Exception($connection->error);
                }
                $connection->close();
            }
        }
        catch(Exception $error)
        {
            echo 'Wystapil blad<br>';
            echo 'Number: '.$error->getCode().'<br>';
            echo 'Information: '.$error->getMessage();
        }
    }

    public function getData(int $uid)
    {
        try
        {
            $connection = new mysqli('localhost', 'patryk','11441','esport');
            if($connection->connect_errno != 0)
            {
                throw new Exception($connection->connect_errno);
            }
            else
            {
                if($result = $connection->query("SELECT * FROM users WHERE uid='$connection->real_escape_string($uid)'"))
                {
                    if($result->num_rows > 0)
                    {
                        $row = $result->fetch_assoc();

                        $this->login        =     $row['username'];
                        $this->email        =     $row['email'];
                        $this->sex          =     $row['sex'];
                        $this->keyRecover   =     $row['key_recovery'];
                        $this->keyDate      =     $row['key_date'];
                        $this->steamid      =     $row['steamid'];
                        $this->steamName    =     $row['steam_name'];
                    }
                    $result->free_result();
                }
                else
                {
                    throw new Exception($connection->error);
                }
                $connection->close();
            }
        }
        catch(Exception $error)
        {
            echo 'Wystapil blad<br>';
            echo 'Number: '.$error->getCode().'<br>';
            echo 'Information: '.$error->getMessage();
        }
    }
}