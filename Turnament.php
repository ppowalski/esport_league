<?php
/**
 * Created by PhpStorm.
 * User: albert
 * Date: 17.12.17
 * Time: 10:18
 */

class Turnament
{
    private $name;
    private $game;
    private $slots;
    private $mode;
    private $leagueMatch;
    private $avards;
    private $endJoin;

    public function getName(): string
    {
        return $this->name;
    }

    public function GetNumberGame(): int
    {
        return $this->game;
    }

    public function getGame(): string
    {
        if($this->game == 0) return "CS:GO";
        else if ($this->game = 1) return "LoL";
        return "NULL";
    }

    public function getSlots(): int
    {
        return $this->slots;
    }

    public function getNumberMode(): int
    {
        return $this->mode;
    }

    public function getMode(): string
    {
        if($this->mode == 0) return "5 vs 5";
        else if($this->mode == 1) return "4 vs 4";
        else if($this->mode == 2) return "3 vs 3";
        else if($this->mode == 3) return "2 vs 2";
        else if($this->mode == 4) return "1 vs 1";
        return "NULL";
    }

    public function getLeagueMatch(): bool
    {
        return $this->leagueMatch;
    }

    public function getAvards(): string
    {
        return $this->avards;
    }

    public function getEndJoin()
    {
        return $this->endJoin;
    }

    public function createTurnament(string $name, int $game, int $slots, int $mode, bool $leagueMatch, string $avards)
    {
        //DODAĆ endJOIN - WAZNE!!!
        //avards do przerobienia

        $validate = true;

        $name = htmlentities($name, ENT_QUOTES);

        if(strlen($name) > 48 || strlen($name) < 3) //[4-48]
        {
            $validate = false;
        }

        if($game != 1 || $game != 2)
        {
            $validate = false;
        }

        if($slots != 4 || $slots != 8 || $slots != 16 || $slots != 32 || $slots != 64 || $slots != 128)
        {
            $validate = false;
        }

        if($mode != 0 || $mode != 1 || $mode != 2 || $mode != 3 || $mode != 4)
        {
            $validate = false;
        }

        if($leagueMatch != true || $leagueMatch != false)
        {
            $validate = false;
        }
    }

}